package org.hogent.nicolas.loots.scalabletestingconcept;

import org.hogent.nicolas.loots.scalabletestingconcept.models.MyObjectBox;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;
import org.hogent.nicolas.loots.scalabletestingconcept.persistency.Generate;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class App extends android.app.Application{
    private BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();
        boxStore = MyObjectBox.builder().androidContext(App.this).build();

        // Generate database
        Box<Person> personBox = boxStore.boxFor(Person.class);
        Generate.generateDatabase(personBox);
    }

    public BoxStore getBoxStore() {
        return this.boxStore;
    }
}
