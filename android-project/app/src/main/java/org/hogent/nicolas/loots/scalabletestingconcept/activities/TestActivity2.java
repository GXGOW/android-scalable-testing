package org.hogent.nicolas.loots.scalabletestingconcept.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import org.hogent.nicolas.loots.scalabletestingconcept.R;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestActivity2 extends AppCompatActivity {

    @BindView(R.id.editText_test2)
    EditText text;

    @BindView(R.id.resultText_test2)
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        ButterKnife.bind(this);
        text.requestFocus();
    }

    @OnClick(R.id.btn_confirm)
    public void checkInput(View view) {
        String input = text.getText().toString();
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        boolean result = Pattern.matches("[a-zA-Z0-9]*", input);
        if(result) {
            textView.setText(R.string.success);
            textView.setTextColor(Color.GREEN);
        } else {
            textView.setText(R.string.failure);
            textView.setTextColor(Color.RED);
        }
    }
}
