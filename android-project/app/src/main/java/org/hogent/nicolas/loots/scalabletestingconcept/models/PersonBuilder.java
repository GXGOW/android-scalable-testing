package org.hogent.nicolas.loots.scalabletestingconcept.models;

public class PersonBuilder {
    String firstName;
    String lastName;
    String address;
    String phoneNumber;
    int image;

    boolean gender;

    public static PersonBuilder personBuilder() {
        return new PersonBuilder();
    }

    public PersonBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public PersonBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public PersonBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    public PersonBuilder withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public PersonBuilder withImage(int image) {
        this.image = image;
        return this;
    }

    public PersonBuilder withGender(boolean gender) {
        this.gender = gender;
        return this;
    }

    public Person build() {
        return new Person(this);
    }
}
