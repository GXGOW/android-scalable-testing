package org.hogent.nicolas.loots.scalabletestingconcept.models;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Person implements Serializable {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;

    private int image;

    private boolean gender; // 0 = female, 1 = male

    Person() {
        // Required empty constructor
    }

    Person(String firstName, String lastName, String address, String phoneNumber, int image, boolean gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.gender = gender;
    }

    Person(PersonBuilder builder) {
        if (builder.firstName == null) throw new NullPointerException("Person must have a first name");
        if (builder.lastName == null) throw new NullPointerException("Person must have a first name");
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.address = builder.address;
        this.phoneNumber = builder.phoneNumber;
        this.image = builder.image;
        this.gender = builder.gender;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }
}
