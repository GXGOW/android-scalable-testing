package org.hogent.nicolas.loots.scalabletestingconcept.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import org.hogent.nicolas.loots.scalabletestingconcept.R;

import java.util.List;

public class ItemAdapter extends ArrayAdapter<String> {
    private final List<String> items;

    public ItemAdapter(Context context, List<String> items) {
        super(context, R.layout.listview_item, items);
        this.items = items;
    }

    public List<String> getItems() {
        return this.items;
    }
}
