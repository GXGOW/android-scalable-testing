package org.hogent.nicolas.loots.scalabletestingconcept.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.adapter.ItemAdapter;
import org.hogent.nicolas.loots.scalabletestingconcept.fragments.DialogAddFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestActivity4 extends AppCompatActivity implements DialogAddFragment.AddListener {

    @BindView(R.id.lv_items)
    ListView listView;

    private ItemAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test4);
        ButterKnife.bind(this);
        List<String> list = new ArrayList<>();
        for(int i = 0; i < 20; i++) {
            list.add("Item "+i);
        }
        adapter = new ItemAdapter(this, list);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener((parent, view, position, id) -> {
            String item = (String) listView.getItemAtPosition(position);
            adapter.remove(item);
            return true;
        });
    }

    @OnClick(R.id.fab)
    public void onFABClick(View view) {
        DialogAddFragment fragment = new DialogAddFragment();
        fragment.show(getSupportFragmentManager(), "addDialog");
    }

    @Override
    public void onAdd(String item) {
        adapter.add(item);
    }
}
