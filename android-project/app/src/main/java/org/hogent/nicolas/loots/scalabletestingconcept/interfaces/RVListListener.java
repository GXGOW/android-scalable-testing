package org.hogent.nicolas.loots.scalabletestingconcept.interfaces;


import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;

public interface RVListListener {
    void onListInteraction(Person person);
}
