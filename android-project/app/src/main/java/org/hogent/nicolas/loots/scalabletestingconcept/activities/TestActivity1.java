package org.hogent.nicolas.loots.scalabletestingconcept.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.hogent.nicolas.loots.scalabletestingconcept.R;

public class TestActivity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);
    }
}
