package org.hogent.nicolas.loots.scalabletestingconcept.adapter.viewholders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.person_image)
    ImageView imageView;

    @BindView(R.id.charView)
    CardView cardView;

    @BindView(R.id.tv_name)
    TextView name;

    @BindView(R.id.tv_phoneNumber)
    TextView phoneNumber;

    private long id;

    public PersonViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void initCard(Person person) {
        this.id = person.getId();
        name.setText(String.format("%s %s", person.getFirstName(), person.getLastName()));
        phoneNumber.setText(person.getPhoneNumber());
        // Resize image
        Picasso.with(imageView.getContext()).load(person.getImage())
                .resize(300,300)
                .centerCrop()
                .into(imageView);
    }
}
