package org.hogent.nicolas.loots.scalabletestingconcept.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.hogent.nicolas.loots.scalabletestingconcept.App;
import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.adapter.PersonAdapter;
import org.hogent.nicolas.loots.scalabletestingconcept.interfaces.OnFABInteractionListener;
import org.hogent.nicolas.loots.scalabletestingconcept.interfaces.RVListListener;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person_;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.objectbox.Box;
import jp.wasabeef.recyclerview.animators.LandingAnimator;


public class PersonRVFragment extends Fragment {

    @BindView(R.id.rv_persons)
    RecyclerView recyclerView;

    private OnFABInteractionListener fabListener;
    private RecyclerView.OnScrollListener fabBehavior;
    private RVListListener rvListener;
    private Box<Person> box;

    private ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    // Remove given character from the adapter
                    PersonAdapter adapter = ((PersonAdapter)recyclerView.getAdapter());
                    int toRemove = viewHolder.getAdapterPosition();
                    Person person = adapter.removeItemAt(toRemove);
                    box.remove(person);
                    adapter.notifyItemRemoved(toRemove);
                }
            };

    public PersonRVFragment() {
        // Required empty constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnFABInteractionListener) fabListener = (OnFABInteractionListener) context;
        else throw new UnsupportedOperationException("OnFABInteractionListener not implemented");
        if (context instanceof RVListListener)
            rvListener = (RVListListener) context;
        else
            throw new UnsupportedOperationException("OnListInteraction not implemented");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rvListener = null;
        fabListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_person_rv, container, false);
        ButterKnife.bind(this,v);
        return v;
    }

    // Initialize RecyclerView on creation
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItemTouchHelper helper = new ItemTouchHelper(simpleItemTouchCallback);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.clearOnScrollListeners();
        recyclerView.addOnScrollListener(fabBehavior);
        LandingAnimator animator = new LandingAnimator();
        animator.setRemoveDuration(300);
        recyclerView.setItemAnimator(animator);
        helper.attachToRecyclerView(recyclerView);

        this.box = ((App)getActivity().getApplication()).getBoxStore().boxFor(Person.class);
        List<Person> personList = box.query().order(Person_.firstName).build().find();
        PersonAdapter adapter = new PersonAdapter(personList, getContext(), (v, position) -> {
            Person p = personList.get(position);
            rvListener.onListInteraction(p);
        });
        recyclerView.setAdapter(adapter);
    }

    public void setRVOnScrollListener(RecyclerView.OnScrollListener RVOnScrollListener) {
        this.fabBehavior = RVOnScrollListener;
    }

}
