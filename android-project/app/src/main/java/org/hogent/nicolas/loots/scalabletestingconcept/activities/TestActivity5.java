package org.hogent.nicolas.loots.scalabletestingconcept.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.fragments.PersonDetailFragment;
import org.hogent.nicolas.loots.scalabletestingconcept.fragments.PersonRVFragment;
import org.hogent.nicolas.loots.scalabletestingconcept.interfaces.OnFABInteractionListener;
import org.hogent.nicolas.loots.scalabletestingconcept.interfaces.RVListListener;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestActivity5 extends AppCompatActivity
        implements OnFABInteractionListener, RVListListener {

    @BindView(R.id.fab)
    FloatingActionButton fab;

    // Show/hide FAB on scroll
    private RecyclerView.OnScrollListener fabBehavior = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                fab.show();
            }
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0 || dy < 0 && fab.isShown()) {
                fab.hide();
            }
        }
    };

    private View.OnClickListener onFABClick = click -> onFABInteraction();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test5);
        ButterKnife.bind(this);
        if(getSupportFragmentManager().findFragmentByTag("personList") == null) {
            PersonRVFragment fragment = new PersonRVFragment();
            fragment.setRVOnScrollListener(fabBehavior);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_test_5, fragment, "personList").commit();
        }
        fab.setOnClickListener(onFABClick);
    }

    @Override
    public void onFABInteraction() {
        PersonDetailFragment pdf = new PersonDetailFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_test_5, pdf, "detailView")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void hideFAB() {
        fab.hide();
    }

    @Override
    public void showFAB() {
        fab.show();
    }

    @Override
    public void onListInteraction(Person person) {
        PersonDetailFragment pdf = new PersonDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("person", person);
        pdf.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_test_5, pdf, "detailView")
                .addToBackStack(null)
                .commit();
    }
}
