package org.hogent.nicolas.loots.scalabletestingconcept.interfaces;

public interface OnFABInteractionListener {
    void onFABInteraction();
    void hideFAB();
    void showFAB();
}
