package org.hogent.nicolas.loots.scalabletestingconcept.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.adapter.viewholders.PersonViewHolder;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;

import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonViewHolder> {

    private List<Person> persons;
    private Context context;
    private CustomItemClickListener listener;

    public PersonAdapter(List<Person> persons, Context context, CustomItemClickListener listener) {
        this.persons = persons;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.cardview_person, parent, false);
        PersonViewHolder personViewHolder = new PersonViewHolder(v);
        // Call parent activity function on click
        v.setOnClickListener(click -> {
            listener.onItemClick(v, personViewHolder.getAdapterPosition());
        });
        return personViewHolder;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.initCard(persons.get(position));
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    public Person getItemAt(int id) {
        return persons.get(id);
    }

    public Person removeItemAt(int id) {
        return persons.remove(id);
    }

    public interface CustomItemClickListener {
        void onItemClick(View v, int position);
    }
}
