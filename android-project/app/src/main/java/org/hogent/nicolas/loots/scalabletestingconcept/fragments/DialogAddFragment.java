package org.hogent.nicolas.loots.scalabletestingconcept.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;

public class DialogAddFragment extends DialogFragment {

    private AddListener listener;
    private Dialog dialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final TextInputEditText text = new TextInputEditText(getContext());
        text.requestFocus();
        builder.setTitle("Add a new item").setView(text)
                .setPositiveButton("Add", (dialog, which) -> {
                    text.clearFocus();
                    String input = text.getText().toString();
                    if(input.trim().equals("")) {
                        showErrorDialog();
                    }
                    else listener.onAdd(text.getText().toString());
                })
                .setNegativeButton("Cancel", ((dialog, which) -> {
                    text.clearFocus();
                    dialog.dismiss();
                }));
        dialog = builder.create();
        return dialog;
    }

    private void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("This field is required")
                .setPositiveButton("OK", (dialog, which) -> {
                    this.dialog.show();
                });
        builder.create().show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AddListener) listener = (AddListener) context;
        else throw new UnsupportedOperationException("AddListener implemented yet");
    }

    public interface AddListener {
        void onAdd(String item);
    }
}
