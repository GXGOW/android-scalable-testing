package org.hogent.nicolas.loots.scalabletestingconcept.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import org.hogent.nicolas.loots.scalabletestingconcept.App;
import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.interfaces.OnFABInteractionListener;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;
import org.hogent.nicolas.loots.scalabletestingconcept.models.PersonBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;


public class PersonDetailFragment extends Fragment {

    @BindView(R.id.et_firstName)
    EditText firstName;

    @BindView(R.id.et_lastName)
    EditText lastName;

    @BindView(R.id.et_address)
    EditText address;

    @BindView(R.id.et_phoneNumber)
    EditText phoneNumber;

    @BindView(R.id.sw_gender)
    Switch gender;

    private Long id;

    private OnFABInteractionListener listener;

    public PersonDetailFragment() {
        // Required empty constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFABInteractionListener) {
            listener = (OnFABInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener.showFAB();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_person_detail, container, false);
        ButterKnife.bind(this, v);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Person person = (Person) bundle.getSerializable("person");
            this.id = person.getId();
            firstName.setText(person.getFirstName());
            lastName.setText(person.getLastName());
            address.setText(person.getAddress());
            phoneNumber.setText(person.getPhoneNumber());
            gender.setChecked(person.isGender());
        }
        listener.hideFAB();

        return v;
    }

    private boolean checkRequiredFields() {
        boolean check = true;
        if (firstName.getText() == null ||
                firstName.getText().toString().trim().equals("")) {
            firstName.setError(
                    getContext()
                            .getResources()
                            .getString(R.string.required)
            );
            check = false;
        }
        if (lastName.getText() == null ||
                lastName.getText().toString().trim().equals("")) {
            lastName.setError(
                    getContext()
                            .getResources()
                            .getString(R.string.required)
            );
            check = false;
        }
        if (phoneNumber.getText() != null &&
                phoneNumber.getText().toString().length() > 10) {
            phoneNumber.setError(
                    getContext()
                            .getResources()
                            .getString(R.string.phone_invalid)
            );
            check = false;
        }
        return check;
    }

    @OnClick(R.id.btn_save)
    public void savePerson() {
        if (checkRequiredFields()) {
            Box<Person> box =
                    ((App) getActivity()
                            .getApplication())
                            .getBoxStore().boxFor(Person.class);
            Person person;
            if (id == null) {
                person = new PersonBuilder()
                        .withFirstName(firstName.getText().toString())
                        .withLastName(lastName.getText().toString())
                        .withAddress(address.getText().toString())
                        .withPhoneNumber(phoneNumber.getText().toString())
                        .withImage(R.drawable.profile)
                        .withGender(gender.isChecked())
                        .build();
            } else {
                person = box.get(id);
                person.setFirstName(firstName.getText().toString());
                person.setLastName(lastName.getText().toString());
                person.setAddress(address.getText().toString());
                person.setPhoneNumber(phoneNumber.getText().toString());
                person.setGender(gender.isChecked());
            }
            box.put(person);
            Toast.makeText(getContext(),
                    "Person successfully saved!", Toast.LENGTH_SHORT).show();
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    }
}
