package org.hogent.nicolas.loots.scalabletestingconcept.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import org.hogent.nicolas.loots.scalabletestingconcept.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestActivity3 extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    @BindView(R.id.imageView_test3)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test3);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_clear)
    public void clearImageView() {
        imageView.setImageDrawable(null);
    }

    @OnClick(R.id.btn_camera)
    public void openCamera() {
        // Check if camera permission has been granted before opening camera
        // If there was no permission granted, request it again
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            // Open camera if permission has been granted
            // If denied, a toast message will be shown
            case REQUEST_IMAGE_CAPTURE: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                }
                else {
                    Toast.makeText(this, "Permission denied.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if(extras != null) {
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imageBitmap = Bitmap.createScaledBitmap(imageBitmap, imageView.getWidth(), imageView.getHeight(), false);
                imageView.setImageBitmap(imageBitmap);
            }
        }
    }
}
