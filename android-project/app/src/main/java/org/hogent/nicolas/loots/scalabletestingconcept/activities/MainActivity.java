package org.hogent.nicolas.loots.scalabletestingconcept.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.hogent.nicolas.loots.scalabletestingconcept.App;
import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;
import org.hogent.nicolas.loots.scalabletestingconcept.persistency.Generate;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_test1)
    public void openActivity1() {
        Intent intent = new Intent(this, TestActivity1.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_test2)
    public void openActivity2() {
        Intent intent = new Intent(this, TestActivity2.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_test3)
    public void openActivity3() {
        Intent intent = new Intent(this, TestActivity3.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_test4)
    public void openActivity4() {
        Intent intent = new Intent(this, TestActivity4.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_test5)
    public void openActivity5() {
        Intent intent = new Intent(this, TestActivity5.class);
        startActivity(intent);
    }
}
