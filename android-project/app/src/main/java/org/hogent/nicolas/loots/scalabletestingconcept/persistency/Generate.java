package org.hogent.nicolas.loots.scalabletestingconcept.persistency;

import org.hogent.nicolas.loots.scalabletestingconcept.R;
import org.hogent.nicolas.loots.scalabletestingconcept.models.Person;

import java.security.SecureRandom;
import java.util.Locale;

import io.objectbox.Box;

import static org.hogent.nicolas.loots.scalabletestingconcept.models.PersonBuilder.personBuilder;

public class Generate {
    public static void generateDatabase(Box<Person> box) {
        if (box.count() > 0) {
            box.removeAll();
        }
        for (int i = 1; i <= 57; i++) {
            SecureRandom random = new SecureRandom();
            Person person = personBuilder()
                    .withFirstName("Person" + i)
                    .withLastName("Personssen" + i)
                    .withAddress(String.format(Locale.ENGLISH, "Sample street %d\n%d Sample City", random.nextInt(100), random.nextInt(10000)))
                    .withPhoneNumber(String.format(Locale.ENGLISH, "0%d", random.nextInt(999999999)))
                    .withImage(R.drawable.profile)
                    .withGender(random.nextBoolean())
                    .build();
            box.put(person);
        }

    }
}
