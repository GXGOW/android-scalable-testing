package org.hogent.nicolas.loots.scalabletestingconcept;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hogent.nicolas.loots.scalabletestingconcept.activities.MainActivity;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.security.SecureRandom;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TestSet2 {

    @Rule
    public IntentsTestRule<MainActivity> intentsRule = new IntentsTestRule<>(MainActivity.class);

    @Test
    public void testInput_valid() {
        testInput("Sample", R.string.success);
    }

    @Test
    public void testInput_invalid() {
        testInput("Sample text", R.string.failure);
    }

    @Test
    public void testInput_invalid_failsRandomly() {
        final boolean willFail = new SecureRandom().nextBoolean();
        final int messageId = willFail ? R.string.success : R.string.failure;
        testInput("This test fails randomly", messageId);
    }

    private void testInput(String toBeTyped, int messageId) {
        onView(withId(R.id.btn_test2)).perform(click());
        onView(withId(R.id.editText_test2)).perform(typeText(toBeTyped));
        onView(withId(R.id.btn_confirm)).perform(click());
        onView(withText(messageId)).check(matches(isDisplayed()));
    }
}
