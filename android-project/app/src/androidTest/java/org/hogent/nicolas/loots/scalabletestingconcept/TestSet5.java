package org.hogent.nicolas.loots.scalabletestingconcept;

import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hogent.nicolas.loots.scalabletestingconcept.activities.MainActivity;
import org.hogent.nicolas.loots.scalabletestingconcept.adapter.viewholders.PersonViewHolder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(AndroidJUnit4.class)
public class TestSet5 {
    private final static String FIRSTNAME = "Bart";
    private final static String LASTNAME = "de Vries";
    private final static String ADDRESS = "Nieuwstraat 21, 9000 Gent";
    private final static String PHONENUMBER_WRONG = "04000123456678";
    private final static String PHONENUMBER_RIGHT = "0474123456";

    private final static String FIRSTNAME_EDIT = "Erik";

    @Rule
    public IntentsTestRule<MainActivity> intentsRule = new IntentsTestRule<>(MainActivity.class);

    // Source: https://alexander-thiele.blogspot.be/2016/01/espresso-ui-tests-and-recyclerview.html
    public static Matcher<RecyclerView.ViewHolder> withHolderPersonView(final String text) {
        return new BoundedMatcher<RecyclerView.ViewHolder,
                PersonViewHolder>(PersonViewHolder.class) {

            @Override
            public void describeTo(Description description) {
                description.appendText("ViewHolder found with text: " + text);
            }

            @Override
            protected boolean matchesSafely(PersonViewHolder item) {
                TextView nameViewText = item.itemView.findViewById(R.id.tv_name);
                return nameViewText != null && nameViewText.getText()
                        .toString().contains(text);
            }
        };
    }

    @Test
    public void testRemoveItem() {
        final int RV_LENGTH = 57;

        launchActivity();
        onView(withId(R.id.rv_persons))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1, swipeRight()));
        onView(withId(R.id.rv_persons))
                .check(new RecyclerViewItemCountAssertion(RV_LENGTH - 1));
    }

    @Test
    public void testAddItem_lastNameRequired() {
        final Resources resources = InstrumentationRegistry
                .getTargetContext().getResources();
        final String errorMessage = resources.getString(R.string.required);

        launchAndAdd();
        onView(withId(R.id.et_firstName))
                .perform(typeText(FIRSTNAME))
                .perform(closeSoftKeyboard());
        save();
        onView(withId(R.id.et_lastName))
                .check(matches(hasErrorText(errorMessage)));
    }

    @Test
    public void testAddItem_firstNameRequired() {
        final Resources resources = InstrumentationRegistry.getTargetContext().getResources();
        final String errorMessage = resources.getString(R.string.required);

        launchAndAdd();
        onView(withId(R.id.et_lastName))
                .perform(typeText(LASTNAME)).perform(closeSoftKeyboard());
        save();
        onView(withId(R.id.et_firstName))
                .check(matches(hasErrorText(errorMessage)));
    }

    @Test
    public void testAddItem_invalidPhoneNumber() {
        final Resources resources = InstrumentationRegistry.getTargetContext().getResources();
        final String errorMessage = resources.getString(R.string.phone_invalid);

        launchAndAdd();
        onView(withId(R.id.et_firstName)).perform(typeText(FIRSTNAME));
        onView(withId(R.id.et_lastName)).perform(typeText(LASTNAME));
        onView(withId(R.id.et_address)).perform(typeText(ADDRESS));
        onView(withId(R.id.et_phoneNumber))
                .perform(typeText(PHONENUMBER_WRONG))
                .perform(closeSoftKeyboard());
        save();
        onView(withId(R.id.et_phoneNumber))
                .check(matches(hasErrorText(errorMessage)));
    }

    @Test
    public void testAddItem_correctInput() {
        launchAndAdd();
        onView(withId(R.id.et_firstName)).perform(typeText(FIRSTNAME));
        onView(withId(R.id.et_lastName)).perform(typeText(LASTNAME));
        onView(withId(R.id.et_phoneNumber))
                .perform(typeText(PHONENUMBER_RIGHT))
                .perform(closeSoftKeyboard());
        save();
        onView(withId(R.id.rv_persons))
                .perform(RecyclerViewActions
                        .scrollToHolder(withHolderPersonView(FIRSTNAME)));
    }

    @Test
    public void testEditItem() {
        launchActivity();
        onView(withId(R.id.rv_persons))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition(3, click()));
        onView(withId(R.id.et_firstName))
                .perform(clearText()).perform(typeText(FIRSTNAME_EDIT))
                .perform(closeSoftKeyboard());
        save();
        onView(withId(R.id.rv_persons))
                .perform(RecyclerViewActions.scrollToHolder
                        (withHolderPersonView(FIRSTNAME_EDIT)));
    }

    private void launchActivity() {
        onView(withId(R.id.btn_test5)).perform(click());
    }

    private void launchAndAdd() {
        launchActivity();
        onView(withId(R.id.fab)).perform(click());
    }

    private void save() {
        onView(withId(R.id.btn_save)).perform(click());
    }
}

/**
 * Custom RecyclerView length matcher
 * Source: https://github.com/Nammari/Weatherify/blob/master/app/src/androidTest/java/nammari/weatherify/RecyclerViewItemCountAssertion.java
 */
class RecyclerViewItemCountAssertion implements ViewAssertion {
    private final int expectedCount;

    RecyclerViewItemCountAssertion(int expectedCount) {
        this.expectedCount = expectedCount;
    }

    @Override
    public void check(View view, NoMatchingViewException noViewFoundException) {
        if (noViewFoundException != null) {
            throw noViewFoundException;
        }
        RecyclerView recyclerView = (RecyclerView) view;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        assertThat(adapter.getItemCount(), is(expectedCount));
    }
}