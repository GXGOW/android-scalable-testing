package org.hogent.nicolas.loots.scalabletestingconcept;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.ImageView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hogent.nicolas.loots.scalabletestingconcept.activities.MainActivity;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.stream.Collectors;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;
import static org.hogent.nicolas.loots.scalabletestingconcept.DrawableMatcher.hasDrawable;

@RunWith(AndroidJUnit4.class)
public class TestSet3 {

    @Rule
    public IntentsTestRule<MainActivity> intentsRule = new IntentsTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule cameraRule = GrantPermissionRule.grant(Manifest.permission.CAMERA);

    @Test
    public void testImportAndClearPicture() {
        // Some vendors use a camera app different from the stock Android camera app
        // This will be checked before executing this instrumentation test
        PackageManager pm = getContext().getPackageManager();
        List<String> packages = pm.getInstalledApplications(0).stream()
                .map(info -> info.packageName).collect(Collectors.toList());
        // Vendor-specific camera package names
        final String[] cameraPackages = {
                "com.android.camera",
                "com.android.camera2",
                "org.lineageos.snap",
                "com.huawei.camera",
                "com.sec.android.app.camera",
                "com.htc.camera",
                "com.asus.camera"};
        String cameraName = "";

        for (String camera : cameraPackages) {
            if (packages.contains(camera)) {
                cameraName = camera;
                break;
            }
        }

        onView(withId(R.id.btn_test3)).perform(click());
        Intent data = new Intent();
        Bitmap image = BitmapFactory.decodeResource(
                InstrumentationRegistry.getTargetContext().getResources(),
                R.drawable.profile);
        data.putExtra("data", image);
        Instrumentation.ActivityResult activity
                = new Instrumentation.ActivityResult(Activity.RESULT_OK, data);
        intending(toPackage(cameraName)).respondWith(activity);
        onView(withId(R.id.btn_camera)).perform(click());
        intended(toPackage(cameraName));
        onView(withId(R.id.imageView_test3)).check(matches(hasDrawable()));
        onView(withId(R.id.btn_clear)).perform(click());
        onView(withId(R.id.imageView_test3)).check(matches(not(hasDrawable())));
    }
}


/**
 * Custom DrawableMatcher
 * Source: https://medium.com/@dbottillo/android-ui-test-espresso-matcher-for-imageview-1a28c832626f
 */
class DrawableMatcher extends TypeSafeMatcher<View> {

    private static final int EMPTY = -1;
    private static final int ANY = -2;

    private final int expectedId;
    private String resourceName;

    private DrawableMatcher(int expectedId) {
        super(View.class);
        this.expectedId = expectedId;
    }

    static Matcher<View> hasDrawable() {
        return new DrawableMatcher(DrawableMatcher.ANY);
    }

    @Override
    protected boolean matchesSafely(View target) {
        if (!(target instanceof ImageView)) {
            return false;
        }
        ImageView imageView = (ImageView) target;
        if (expectedId == EMPTY) {
            return imageView.getDrawable() == null;
        }
        if (expectedId == ANY) {
            return imageView.getDrawable() != null;
        }

        Resources resources = target.getContext().getResources();
        Drawable expectedDrawable = resources.getDrawable(expectedId, null);
        resourceName = resources.getResourceEntryName(expectedId);

        if (expectedDrawable == null) {
            return false;
        }

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Bitmap otherBitmap = ((BitmapDrawable) expectedDrawable).getBitmap();
        return bitmap.sameAs(otherBitmap);
    }


    @Override
    public void describeTo(Description description) {
        description.appendText("with drawable from resource id: ");
        description.appendValue(expectedId);
        if (resourceName != null) {
            description.appendText("[");
            description.appendText(resourceName);
            description.appendText("]");
        }
    }
}