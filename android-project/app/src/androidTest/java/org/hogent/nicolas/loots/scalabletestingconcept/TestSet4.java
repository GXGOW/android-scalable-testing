package org.hogent.nicolas.loots.scalabletestingconcept;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.AdapterView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hogent.nicolas.loots.scalabletestingconcept.activities.MainActivity;
import org.hogent.nicolas.loots.scalabletestingconcept.adapter.ItemAdapter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.security.SecureRandom;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.StringEndsWith.endsWith;

@RunWith(AndroidJUnit4.class)
public class TestSet4 {

    @Rule
    public IntentsTestRule<MainActivity> intentsRule = new IntentsTestRule<>(MainActivity.class);

    private static Matcher<View> itemExists(final Matcher<String> nameMatcher) {
        return new TypeSafeMatcher<View>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("with class name: ");
                nameMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof AdapterView)) {
                    return false;
                }

                ItemAdapter adapter = (ItemAdapter) ((AdapterView) view).getAdapter();
                List<String> items = adapter.getItems();
                for (String item : items) {
                    if (nameMatcher.matches(item)) {
                        return true;
                    }
                }

                return false;
            }
        };
    }

    @Test
    public void testRemoveItem() {
        final String item = "Item 3";
        launchActivity();
        onView(withText(item)).perform(longClick());
        onView(withText(item)).check(doesNotExist());
    }

    @Test
    public void testAddItem() {
        final String input = "New character";
        launchActivity();
        onView(withId(R.id.fab)).perform(click());
        onView(allOf(withClassName(endsWith("EditText"))))
                .perform(typeText(input));
        onView(withText("Add")).perform(click());
        onView(withId(R.id.lv_items)).check(matches(itemExists(is(input))));
    }

    @Test
    public void testAddItem_failsRandomly() {
        final boolean willFail = new SecureRandom().nextBoolean();
        final String input = "New character";
        launchActivity();
        onView(withId(R.id.fab)).perform(click());
        onView(allOf(withClassName(endsWith("EditText"))))
                .perform(typeText(willFail ? input : "Failure"));
        onView(withText("Add")).perform(click());
        onView(withId(R.id.lv_items)).check(matches(itemExists(is(input))));
    }

    private void launchActivity() {
        onView(withId(R.id.btn_test4)).perform(click());
    }
}