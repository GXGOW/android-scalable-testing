# Bachelorproef toegepaste informatica

## Scalabale Android testing: een vergelijkende studie

Op deze repository kan je de source-bestanden vinden van de vergelijkende studie tussen de drie parallel testing frameworks
[Spoon](https://github.com/square/spoon), [Composer](https://github.com/gojuno/composer) en [Firebase Test Lab](https://firebase.google.com/docs/test-lab/). Deze repository bestaat uit een aantal delen delen:

* [Het onderzoeksvoorstel](./voorstel)
* [De broncode van de scriptie zelf zelf](./bachproef).
* [De broncode van de poster](./poster) die werd uitgehangen op de dag van de bachelorproefverdediging.
* [De broncode van de Android-applicatie](./android-project) die werd gebruikt om de frameworks met elkaar te vergelijken.

De gecompileerde versies van bovenstaande items zijn [hier terug te vinden](https://gitlab.com/GXGOW/android-scalable-testing/tags/1.0.1).

De gebruikte frameworks zijn via volgende links te verkrijgen:

* [Spoon](https://square.github.io/spoon/#download) (Runner JAR)
* [Composer](https://github.com/gojuno/composer#download)

## Hoe compileren

### Lokale LaTeX-distributie

Hiervoor is volgende software vereist:

* LaTeX-distributie naar keuze (TeX Live, MiKTeX, ...)
* [Python](https://www.python.org/downloads/)
* [Pygments](http://pygments.org/download/) (voor compilate van broncode)

Als er aan deze voorwaarden voldaan is, kunnen de tex-bestanden gecompileerd worden met behulp van [build-latex.sh](./build-latex.sh).

### Docker

Een alternative methode is het gebruik van een Docker-container. Hiervoor is enkel de installatie van [Docker](https://store.docker.com/search?type=edition&offering=community) vereist. Daarna kunnen de tex-documenten gecompileerd worden met

```bash
docker build -t latex-compiler . && docker run --rm -v "$(pwd)":/mnt/data --name bachproef-builder latex-compiler bash -c "/mnt/data/build-latex.sh"
```