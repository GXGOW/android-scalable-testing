library(readr)
firebase_results <- read_csv("csv/firebase-results.csv")
firebase_results_5X <- read_csv("csv/firebase-results_5X.csv")

one_device <- firebase_results$Uitvoeringstijd[1]
one_device_5X <- firebase_results_5X$Uitvoeringstijd[1]

three_devices <- firebase_results$Uitvoeringstijd[3]
three_devices_5X <- firebase_results_5X$Uitvoeringstijd[2]

six_devices <- firebase_results$Uitvoeringstijd[6]
six_devices_5X <- firebase_results_5X$Uitvoeringstijd[3]

combined_table <- matrix(c(one_device, one_device_5X, three_devices, three_devices_5X, six_devices, six_devices_5X), ncol=6)
colnames(combined_table) <- c("1 apparaat","1 apparaat (5X)","3 apparaten","3 apparaten (5X)","6 apparaten","6 apparaten (5X)")
rownames(combined_table) <- "Uitvoeringstijd (in s)"
png('bachproef/img/tests/results_firebase_5X.png', width= 800, height=600)
bp <- barplot(combined_table, ylim=c(0,650), main="Uitvoeringstijden", sub = "5X verwijst naar de vervanging van de Nexus 6P (API 24) door de Nexus 5X (API 27)", 
              col="lightblue", ylab="Uitvoeringstijd (in s)")
all_values <- as.vector(combined_table[1,]) 
text(x= bp, y= all_values, label = all_values, pos=3, col=c("red","darkgreen","red","darkgreen","red","darkgreen"))
dev.off()