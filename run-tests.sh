#!/bin/bash

## DEVICE ORDER
## FIRST RUN: PXL-6P-5X-GPAD-S7E-TABS2-3S-M9
## SECOND RUN: 3S-M9-S7E-PXL-GPAD-6P-5X-TABS2
## THIRD RUN: TABS2-S7E-6P-3S-M9-PXL-GPAD-5X

# Check arguments
# If no arguments are specified, exit with error code 1
if [[ $# -eq 0 ]] ; then
    echo "No arguments specified. Exiting..."
    exit 1
fi

amount=$1
output="results-thirdrun/devices-$amount"

# Check if test results already exist
# If so, remove directory to avoid conflicts
if [ -d $output ]; then
    rm -rf $output
fi

# Run Spoon tests
echo "Running Spoon..."
time java -jar spoon-runner-1.7.1-jar-with-dependencies.jar \
--apk ./android-project/app/build/outputs/apk/debug/app-debug.apk \
--test-apk ./android-project/app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk | tee spoon-debug.txt
echo "Finished running Spoon."

# Run composer tests
echo "Running Composer..."
time java -jar composer-0.3.3.jar \
--apk ./android-project/app/build/outputs/apk/debug/app-debug.apk \
--test-apk ./android-project/app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk \
--shard false | tee composer-debug.txt
echo "Finished running Composer."

# Move to designated folder
mkdir $output
mv *-output $output
mv *-debug.txt $output
