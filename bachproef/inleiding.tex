%%=============================================================================
%% Inleiding
%%=============================================================================

\chapter{Inleiding}\label{ch:inleiding}

In dit hoofdstuk zal er kort uitgelegd worden wat de hedendaagse problematiek is die ervoor zorgt dat dit onderzoek 
noodzakelijk is. Daarnaast wordt er nog kort uitgelegd wat dit onderzoek specifiek omvat en wat de volgende hoofdstukken omvatten.

\section{Probleemstelling}\label{sec:probleemstelling}

Android is wereldwijd het meest-gebruikte mobiele besturingssysteem voor smartphones en tablets. Begin 2017 had Android een wereldwijd marktaandeel
van 85\% onder de mobiele apparaten, goed voor meer dan twee miljard actieve Android-apparaten \autocite{MARKET2017}. Deze zijn gefabriceerd door honderden verschillende smartphonefabrikanten, 
waaronder de alombekende merken als Samsung, Huawei, LG, Sony, HTC, Nokia en nog vele anderen.
Dit vormt voor applicatieontwikkelaars een extra uitdaging naast het bouwen van een robuuste applicatie, aangezien zijn ervoor moeten zorgen dat hun apps
op een zo groot mogelijk deel van deze twee miljard apparaten probleemloos kunnen worden uitgevoerd. Hierbij moet er rekening gehouden worden met volgende punten:
\begin{itemize}
    \item Schermgroottes: Dit kan variëren van de kleine 4-inch smartphones tot de grote 10-inch tablets. Daarnaast is er nog de mogelijkheid om Android-apps te installeren en uit te voeren op laptops en desktop computers
    met behulp van Chromebooks \autocite{CHROMEOS} of het Android-x86 Open Source Project \autocite{ANDROIDX86}. Die laatste is de laatste jaren een nog relatief klein, maar steeds meer groeiend, marktaandeel geworden.
    \item Android-versies: Sinds de meest-recente meting (7 mei 2018) draait slechts 0,8\% van de Android-apparaten op de laatste nieuwe versie, Android 8.1 Oreo, dat uitgebracht werd in december 2017.
    Het grootste deel, 25,5\%, draait op Android 6.0 Marshmallow, uitgebracht eind 2015. Daarop volgt 22,9\% op Android 7.0 Nougat, de vorige grote update \autocite{DISTRIBUTION2018}.
    Deze fragmentatie is vooral te wijten aan de smartphonefabrikanten en hun inconsistentie op vlak van het uitbrengen van updates. Een reden hiervoor is het feit dat het economisch interessanter is om 
    gebruikers aan te zetten om elk jaar een nieuw apparaat te kopen dan tijd en geld te investeren in het ontwikkelen van software- en beveiligingsupdates voor bestaande apparaten.
    \item Skins: Een van de troeven van het Android besturingssysteem is zijn \textit{open source nature}. Dit wil zeggen dat vrijwel elke smartphonefabrikant wijzigingen kan aanbrengen aan het besturingssysteem voordat die het op
    de apparaten verscheept, wat tegelijkertijd ook een nadeel kan zijn. De meest opvallende wijzigingen zijn de zogenoemde \textit{skins}, eigen thema's en applicatiesets van de fabrikant. Sommige fabrikanten maken hier niet zo veel gebruik van 
    en voegen slechts kleine, nuttige extra's toe aan het besturingssysteem. Andere fabrikanten veranderen dan weer het volledige thema en zorgen ervoor dat er voor elke meegeleverde Google-applicatie een zelfontwikelde tegenhanger beschikbaar is.
    \item Specificaties: Android draait op zowel minder performante low-end apparaten als high-end vlaggenschepen. Apps moeten vlot kunnen draaien op zowel trage als snelle apparaten. In combinatie met een gebrek aan updates en \textit{resource-intensive} skins zullen
    grafisch-intensieve apps niet even performant op verschillende apparaten werken door verschillende OpenGL/Vulkan versies en verschillende chipsets (CPU/GPU-combinaties als Snapdragon/Adreno, Helio/Mali, Helio/PowerVR, \ldots)
\end{itemize}

Rekening houden met bovenstaande gegevens kan het leven van een applicatieontwikkelaar een stuk moeilijker maken. In theorie zou men een applicatie op elk bestaand apparaat 
moeten kunnen testen om er zeker van te zijn dat het zonder problemen werkt. In de praktijk is dit vandaag nog zo goed als onrealistisch en onhaalbaar.
Zelfs al zou men de applicatie op een fractie van deze apparaten kunnen uittesten, dan is het evengoed nog een heel tijdrovend en frustrerend proces.
Vooral het \textit{tijdrovende} is in de hedendaagse bedrijfswereld, alsook voor freelancers en \textit{hobbyists}, absoluut niet interessant.

Gelukkig is er ondertussen al een ruim aanbod aan softwareoplossingen op de markt gekomen om dit proces te automatiseren en dus tests automatisch op verschillende apparaten tegelijk uit te voeren.
Natuurlijk is het niet haalbaar om alle beschikbare frameworks te gaan onderzoeken, aangezien dat te ruim is om binnen het kader van dit onderzoek te kunnen passen. Er moeten dus grenzen getrokken worden.
Daarom is ervoor gekozen om binnen dit werk Spoon \autocite{SPOON}, Composer \autocite{COMPOSER} en Firebase Test Lab \autocite{FIREBASE} te onderzoeken.
Waarom specifiek deze frameworks gekozen zijn, komt verder in dit onderzoek aan bod, meer bepaald in~\ref{sec:methodologie}.

\section{Onderzoeksvraag}\label{sec:onderzoeksvraag}

In deze bachelorproef gaat er onderzocht worden welke van de hiervoorgenoemde frameworks het beste is om de probleemstelling op te lossen voor welke use cases.
Dit wordt verder gespecifieerd in volgende criteria en deelvragen:

\begin{itemize}
    \item Snelheid: Hoe lang duurt het om de verschillende tests uit te voeren? Welke impact hebben het toevoegen van apparaten aan de cluster en de complexiteit van de test op de uitvoertijd?
    \item Schaalbaarheid en stabiliteit: Hoe gemakkelijk kunnen er apparaten aan de cluster worden toegevoegd? Heeft dit impact op de performantie van het framework? Lijdt de stabiliteit van het framework hieronder?
    \item Ease-of-use: Hoe eenvoudig kunnen de frameworks geconfigureerd worden en de tests uitgevoerd worden?
    \item Gedetailleerdheid: Hoe gedetailleerd zijn de testresultaten? Hoe eenvoudig kan er getraceerd worden waar testen eventueel zijn foutgelopen en op welke apparaten?
\end{itemize}

Daarnaast wordt er ook gelet op overige criteria die hier niet apart vernoemd worden, maar die evengoed tijdens het uitvoeren van de tests aan bod kunnen komen. Denk hierbij bijvoorbeeld aan \textit{resource usage}, \ldots

\section{Onderzoeksdoelstelling}\label{sec:onderzoeksdoelstelling}

Aan het einde van dit onderzoek zal er duidelijk moeten zijn welke van de drie frameworks het meest geschikt is om in het grootste deel van de use cases 
testen uit te voeren op een snelle en efficiënte manier. Daarnaast moeten er duidelijke testresultaten geleverd worden en moet het mogelijk zijn om probleemloos deze tests uit te voeren
op een variabel aantal Android-apparaten tegelijkertijd.

\section{Opzet van deze bachelorproef}\label{sec:opzet-bachelorproef}

% Het is gebruikelijk aan het einde van de inleiding een overzicht te
% geven van de opbouw van de rest van de tekst. Deze sectie bevat al een aanzet
% die je kan aanvullen/aanpassen in functie van je eigen tekst.

De rest van deze bachelorproef is als volgt opgebouwd:

In Hoofdstuk~\ref{ch:stand-van-zaken} wordt een overzicht gegeven van de stand van zaken binnen het onderzoeksdomein, op basis van een literatuurstudie.

In Hoofdstuk~\ref{ch:methodologie} wordt de methodologie toegelicht en worden de gebruikte onderzoekstechnieken besproken om een antwoord te kunnen formuleren op de onderzoeksvragen.

In Hoofdstuk~\ref{ch:opstelling} wordt de specifieke opstelling van het onderzoek besproken en welke middelen hiervoor gebruikt zijn.

In Hoofdstuk~\ref{ch:resultaten} worden de resultaten van het onderzoek besproken en worden er deelconclusies getrokken.

In Hoofdstuk~\ref{ch:conclusie}, tenslotte, wordt uit de testresultaten een conclusie getrokken en een antwoord geformuleerd op de onderzoeksvragen. Daarbij wordt ook besproken wat het nut ervan is in de hedendaagse context en eventuele toekomstige onderzoeken.

