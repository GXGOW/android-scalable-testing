%%=============================================================================
%% Methodologie
%%=============================================================================

\chapter{Methodologie}\label{ch:methodologie}

%% TODO: Hoe ben je te werk gegaan? Verdeel je onderzoek in grote fasen, en
%% licht in elke fase toe welke stappen je gevolgd hebt. Verantwoord waarom je
%% op deze manier te werk gegaan bent. Je moet kunnen aantonen dat je de best
%% mogelijke manier toegepast hebt om een antwoord te vinden op de
%% onderzoeksvraag.

Dit hoofdstuk gaat dieper in op het voorbereiden van de testopstelling en waarom deze op een bepaalde manier is opgezet. Daarnaast wordt ook de theoretische opbouw van de nodige testapplicatie besproken.

\section{Opbouw testapplicatie}\label{sec:opbouw}

Om de verschillende frameworks te kunnen testen, was er in de eerste plaats een Android-applicatie nodig. Pas wanneer deze ontwikkeld was, konden er hiervoor \Gls{instrumentation}s ontwikkeld worden om te verzekeren dat de applicatie werkt zoals hij 
zou moeten werken.

\textit{"Een Android-applicatie ontwikkelen"} is wel een heel breed begrip, want dit kan op honderden verschillende manieren bereikt worden. Dit kan gaan van een simpele notitieapp tot een complexe social-media-app.
Uiteraard is het niet de bedoeling om binnen dit onderzoek een revolutionaire, innovatieve, nieuwe applicatie te ontwikkelen, aangezien dat niet binnen de doelstelling ervan ligt. Daarom is de applicatie die gebruikt wordt
relatief eenvoudig, maar toch uitgebreid genoeg om zijn nut te kunnen bewijzen binnen het kader van dit onderzoek.

Er werd daarom gekozen om de applicatie op te splitsen in vijf grote delen, elk met een eigen complexiteit en use case. Zo worden zeker de belangrijkste soorten UI-componenten gebruikt en getest.
De volledige uitwerking en de technische opbouw van de applicatie wordt beschreven in Hoofdstuk~\ref{sec:application}. Alle onderdelen worden getest aan de hand van instrumentatietests geschreven met behulp van het Espresso-framework, de de-facto standaard van de 
native Android instrumentatietests.

\section{Opbouw testomgeving}

Vanaf dat de testapplicatie ontwikkeld was, kon de rest van de testomgeving opgebouwd worden. Er werd hierbij gekozen om een combinatie van virtuele apparaten (uitgevoerd met behulp van de Android Virtual Device manager, meegeleverd in Android Studio) en fysieke apparaten te gebruiken. 
De reden dat er voor een combinatie van beide soorten werd gekozen, is omdat hier de schaalbaarheid van de frameworks onderzocht wordt. De frameworks zouden dus met allerlei soorten apparaten moeten kunnen werken, dus zowel met virtuele als fysieke.

Daarnaast kon het testen niet enkel beperkt worden tot virtuele apparaten, omdat deze gelimiteerd werden door de specificaties van de computer waarop ze geëmuleerd werden. Hoe meer apparaten er tegelijkertijd geëmuleerd werden, 
hoe meer kans er was dat de snelheid per apparaat beperkt werd, wat een impact gehad zou kunnen hebben op de uitvoeringssnelheid van de frameworks. Om hiermee rekening te houden, werd er besloten om het aantal virtuele apparaten te limiteren tot drie en deze enkel op te starten 
als er een test op uitgevoerd werd. Als men de testresultaten van bijvoorbeeld twee virtuele apparaten bekijkt, dan mag men ervan uitgaan dat er op dat moment ook effectief slechts twee virtuele apparaten uitgevoerd werden op de testcomputer, dit om 
de uitvoeringssnelheid van de tests niet te beïnvloeden en om niet onnodig processorkracht te verspillen.

Zoals hiervoor werd vernoemd, zijn de frameworks die in dit onderzoek getest worden Spoon, Composer en Firebase Test Lab. Er werd voor deze frameworks gekozen om volgende redenen:

\begin{itemize}
    \item Deze frameworks zijn het meest voor de hand liggend door hun populariteit.
    \item Deze frameworks zijn gratis te verkrijgen en te gebruiken, mits limitaties in sommige gevallen.
    \item Deze frameworks bieden ondersteuning voor \Gls{instrumentation}s geschreven met Espresso
\end{itemize}

Het testen van de frameworks verliep als volgt: er werd een apparaat aangesloten aan de testcomputer, waarna met behulp van een Bash-script Spoon en Composer in deze volgorde na elkaar uitgevoerd werden. Dit werd in sommige gevallen twee of drie keer herhaald vanwege uitzonderlijke problemen
(connectieproblemen, onverklaarbare runtime errors, andere problemen met de testcomputer die de testresultaten beïnvloedden,~\ldots). Over problemen die zich voordeden tijdens het testen, wordt meer uitleg gegeven in Hoofdstuk~\ref{ch:resultaten}.

Aangezien enkel Spoon de totale uitvoeringstijd wegschreef in de testresultaten, was er een andere manier nodig om voor beide frameworks de totale uitvoeringstijd te meten. 
Dit werd bereikt met behulp van het Linux-commando \textit{time}. Dit commando doet het volgende:

\begin{itquote}
    The time command runs the specified program command with the given arguments. When command finishes, time writes a message to standard error giving timing statistics about this program run.
    These statistics consist of (i) the elapsed real time between invocation and termination, (ii) the user CPU time (the sum of the tms\_utime and tms\_cutime values in a struct tms as returned
    by times(2)), and (iii) the system CPU time (the sum of the tms\_stime and tms\_cstime values in a struct tms as returned by times(2)).

     ---\autocite{TIME}
\end{itquote}

In dit geval is enkel het eerste gegeven dat wordt gegenereerd nuttig. Aan de hand hiervan kan namelijk de tijd berekend worden tussen het starten en het stoppen van een van de testing frameworks.

De uitvoeringstijd werd dan per framework vastgelegd in een tabel en de genereerde testresultaten worden per framework en per apparaatcombinatie bijgehouden.
Daarnaast wordt de uitvoer die door beide frameworks gegenereerd worden weggeschreven naar een tekstbestand.
Aangezien dat de apparaten zelf een invloed kunnen hebben op de totale uitvoeringstijd, wordt dit hele proces drie keer herhaald, waarbij elke keer de apparaten in een andere volgorde worden aangesloten. Zo kunnen eventuele verschillen in uitvoeringstijd 
verklaard worden aan de hand van de apparaten die op dat moment gebruikt zijn.

In het geval van Firebase kon deze manier van werken niet volledig hergebruikt worden, dit om volgende redenen:
\begin{itemize}
    \item Firebase Test Lab hanteert een dagelijkse limiet van 10 virtuele en 5 fysieke apparaten per dag. Deze wordt pas gereset om 12:00 PST (9:00u in onze tijdzone).
    Hierdoor werd de dagelijkse limiet al bereikt bij 4 apparaten, waardoor het enkele dagen duurde voordat de eerste testvolgorde voltooid werd. Het hele testproces werd hierdoor maar één keer doorlopen,
    met uitzondering van een paar gevallen. Hierover meer uitleg in Hoofdstuk~\ref{ch:resultaten}.
    \item De totale uitvoeringstijd wordt niet weergegeven door Firebase. Enkel de starttijd werd hiervoor weergegeven, maar dit was in de relatieve vorm \textit{\ldots minuten geleden }, wat niet gedetailleerd genoeg is.
    Om deze reden werd de uitvoeringstijd handmatig gemeten met behulp van een stopwatch. Deze methode was ook niet ideaal, maar wel gedetailleerder dan het noteren van de relatieve starttijd.
    \item Aangezien dit een cloud-based platform is, konden apparaten niet fysiek aangesloten worden. Deze moesten via het webplatform geselecteerd worden. Daarnaast waren niet alle apparaten die 
    gebruikt werden bij het tesproces voor Spoon en Composer beschikbaar bij Firebase. Om deze reden zijn er een aantal apparaten vervangen binnen het tesproces voor Firebase Test Lab. Welke apparaten 
    er precies vervangen zijn, wordt beschreven in Hoofdstuk~\ref{sec:firebase}
\end{itemize}

Afgezien van deze opmerkingen, is het tesproces voor Firebase Test Lab in grote lijnen op dezelfde manier verlopen.
De verschillende overzichten van de testresultaten die gegenereerd werden door deze drie frameworks zijn terug te vinden in Appendix~\ref{ch:testresultaten}.