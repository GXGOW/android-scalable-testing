## Build bachproef
cd bachproef/
latexmk -synctex=1 -interaction=nonstopmode -file-line-error -pdf -shell-escape bachproef-tin.tex
rm bachproef-tin*.pdf
biber bachproef-tin
makeglossaries bachproef-tin
latexmk -synctex=1 -interaction=nonstopmode -file-line-error -pdf -shell-escape bachproef-tin.tex

## Build poster
cd ../poster
latexmk -synctex=1 -interaction=nonstopmode -file-line-error -pdf conference_poster_6.tex

## Build voorstel
cd ../voorstel
latexmk -synctex=1 -interaction=nonstopmode -file-line-error -pdf loots_nicolas_voorstel.tex