# Import dataset
library(readr)
library(matrixStats)
spoon_results <- read_csv("csv/spoon-results.csv")
composer_results <- read_csv("csv/composer-results.csv")
firebase_results <- read_csv("csv/firebase-results.csv")

# Convert to vectors
device_amount <- spoon_results$Apparaten

spoon_run1 <- spoon_results$Uitvoering1
spoon_run2 <- spoon_results$Uitvoering2
spoon_run3 <- spoon_results$Uitvoering3
spoon_runtimes <- spoon_results[,-1]
composer_runtimes <- composer_results[,-1]

composer_run1 <- composer_results$Uitvoering1
composer_run2 <- composer_results$Uitvoering2
composer_run3 <- composer_results$Uitvoering3

time_firebase <- firebase_results$Uitvoeringstijd

time_spoon_allRuntimes <- rowMeans(spoon_runtimes, na.rm = TRUE);
time_composer_allRuntimes <- rowMeans(composer_runtimes, na.rm = TRUE)
# Calculate and add means and standarddeviation to tables
time_spoon_mean <- colMeans(spoon_runtimes, na.rm = TRUE)
time_composer_mean <- colMeans(composer_runtimes, na.rm=TRUE)
time_spoon_sd <- colSds(data.matrix(spoon_runtimes), na.rm=TRUE)
time_composer_sd <- colSds(data.matrix(composer_runtimes), na.rm=TRUE)

time_spoon_totalMean <- mean(as.vector(as.matrix(spoon_runtimes)))
time_spoon_totalSd <- sd(as.vector(as.matrix(spoon_runtimes)))
time_composer_totalMean <- mean(as.vector(as.matrix(composer_runtimes)))
time_composer_totalSd <- sd(as.vector(as.matrix(composer_runtimes)))

spoon_current_mean <- round(time_spoon_totalMean, 3)
spoon_current_sd <- round(time_spoon_totalSd, 3)
composer_current_mean <- round(time_composer_totalMean, 3)
composer_current_sd <- round(time_composer_totalSd, 3)
firebase_mean <- round(mean(time_firebase), 3)
firebase_sd <- round(sd(time_firebase), 3)
# Draw and export graph
y <- seq(from = 0, to = 945, by = 135)
#y <- seq(from = 0, to = 98, by = 14)
png('bachproef/img/tests/results_avgsc_firebase.png', width= 800, height=600)
plot(device_amount,y,type="n",xlab="",ylab="") 
grid(nx = 7, ny=100, lty = "dotted")
title(main = "Uitvoeringstijd per aantal devices (Gemiddelden Spoon en Fork, uitvoeringstijd Firebase)",
      sub=bquote("Spoon("~mu == .(spoon_current_mean)~","~sigma == .(spoon_current_sd)~"), Composer("~mu == .(composer_current_mean)~","~sigma == .(composer_current_sd)~"), Firebase("~mu == .(firebase_mean)~","~sigma == .(firebase_sd)~")"), 
      xlab="Aantal devices", ylab="Uitvoeringstijd (in s)")
lines(device_amount,time_spoon_allRuntimes,type="o", col="green")
lines(device_amount,time_composer_allRuntimes,type="o", col="blue")
lines(device_amount,time_firebase,type="o", col="red")
abline(h = spoon_current_mean, lty=2, col="green")
abline(h = composer_current_mean, lty=2, col="blue")
abline(h = firebase_mean, lty=2, col="red")
#legend(x = 5.5, y=40,legend = c("Spoon","Composer","Spoon (µ)","Composer (µ)"), col=c("green","blue","green","blue"), lty=c(1,1,2,2))
legend(x =6.86, y=400,legend = c("Spoon","Composer","Firebase", "Spoon(µ)", "Composer(µ)", "Firebase(µ)"), col=c("green","blue","red","green","blue","red"), lty=c(1,1,1,2,2,2))
dev.off()
